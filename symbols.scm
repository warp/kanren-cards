(use-modules (srfi srfi-1)
             (srfi srfi-9)
             (ice-9 match))

(define-record-type <player>
  (player name hp)
  player?
  (name player-name)
  (hp player-hp))

(define-record-type <pure>
  (pure element)
  pure?
  (element pure-element))

(define-record-type <symbol>
  (symbol type cont)
  symbol?
  (type symbol-type)
  (cont symbol-cont))

(define (log-effect callee str)
  (format #t "applied effect ~a: ~a\n" callee str))

(define (wound element)
  (match-lambda
    (($ <player> name hp)
     (begin
       (log-effect 'wound (format #f "~a is wounded by ~a" name element))
       (player name (1- hp))))))

(define (kindle k)
  (match-lambda
    (0 k)
    (n
     (lambda (x)
       (((kindle k) (1- n)) (k x))))))

(define (fan k) (lambda (l) (map k l)))

(define flame (pure 'fire))

(define blood
  (symbol 'effect
    (match-lambda
      (($ <pure> element) (symbol 'effect (wound element))))))

(define kindling
  (symbol 'modifier
    (match-lambda
      (($ <symbol> 'effect k) (symbol 'effect ((kindle k) 5))))))

(define wind
  (symbol 'modifier
    (match-lambda
      (($ <symbol> 'effect k) (symbol 'effect (fan k))))))

(define (link sym x)
  ((symbol-cont sym) x))

(define ignite (link blood flame))
(define bonfire (link kindling ignite))
(define flashfire (link wind ignite))
(define wildfire (link wind bonfire))

(define-record-type <card>
  (card fun)
  card?
  (fun card-fun))

(define wand
  (match-lambda
    (($ <symbol> 'effect k) (card k))))

(define ignite-wand (wand ignite))
(define bonfire-wand (wand bonfire))
(define flashfire-wand (wand flashfire))
(define wildfire-wand (wand wildfire))

(define (play card param) ((card-fun card) param))

(define alice (player "alice" 10))
(define ben (player "ben" 7))
(define party (list alice ben))

(define (test-ignite) (begin (display alice) (newline) (play ignite-wand alice)))
(define (test-bonfire) (begin (display ben) (newline) (play bonfire-wand ben)))
(define (test-flashfire) (begin (display party) (newline) (play flashfire-wand party)))
(define (test-wildfire) (begin (display party) (newline) (play wildfire-wand party)))
